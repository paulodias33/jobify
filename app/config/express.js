const express = require('express')
const bodyParser = require('body-parser')
const app = express()

module.exports = function () {
  app.use('/admin', (req, res, next) => {
    if (req.hostname === 'localhost') {
      next()
    } else {
      res.send('Sorry!')
    }
  })

  app.set('view engine', 'ejs')
  app.use(express.static('public'))
  app.use(bodyParser.urlencoded({ extended: true }))

  return app
}
