const dbConnection = require('../models/db')

module.exports = {
  async index (req, res) {
    const db = await dbConnection
    const categorias = await db.all('select * from categorias')

    res.render('admin/categorias', { categorias })
  },

  async showNew (req, res) {
    res.render('admin/nova-categoria')
  },

  async store (req, res) {
    const { categoria } = req.body
    const db = await dbConnection
    await db.run(`insert into categorias (categoria) values ('${categoria}')`)

    res.redirect('/admin/categorias')
  },

  async show (req, res) {
    const { id } = req.params
    const db = await dbConnection
    const categoria = await db.get(`select * from categorias where id = ${id}`)

    res.render('admin/editar-categoria', { categoria })
  },

  async update (req, res) {
    const { categoria } = req.body
    const { id } = req.params
    const db = await dbConnection
    await db.run(
      `update categorias set categoria = '${categoria}' where id = ${id}`
    )

    res.redirect('/admin/categorias')
  },

  async destroy (req, res) {
    const { id } = req.params
    const db = await dbConnection
    await db.run(`delete from categorias where id = ${id}`)

    res.redirect('/admin/categorias')
  }
}
