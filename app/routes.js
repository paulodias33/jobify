const express = require('express')
const routes = express.Router()
const HomeController = require('./controllers/HomeController')
const AdminControllerVaga = require('./controllers/AdminControllerVaga')
const AdminControllerCategoria = require('./controllers/AdminControllerCategoria')

routes.get('/', HomeController.index)
routes.get('/vaga/:id', HomeController.show)

routes.get('/admin', AdminControllerVaga.admin)
routes.get('/admin/vagas', AdminControllerVaga.index)
routes.get('/admin/vagas/editar/:id', AdminControllerVaga.show)
routes.get('/admin/vagas/delete/:id', AdminControllerVaga.destroy)
routes.post('/admin/vagas/editar/:id', AdminControllerVaga.update)
routes.get('/admin/vaga/nova', AdminControllerVaga.showNew)
routes.post('/admin/vaga/nova', AdminControllerVaga.store)

routes.get('/admin/categorias', AdminControllerCategoria.index)
routes.get('/admin/categoria/nova', AdminControllerCategoria.showNew)
routes.post('/admin/categoria/nova', AdminControllerCategoria.store)
routes.get('/admin/categoria/editar/:id', AdminControllerCategoria.show)
routes.post('/admin/categoria/editar/:id', AdminControllerCategoria.update)
routes.get('/admin/categoria/delete/:id', AdminControllerCategoria.destroy)

module.exports = routes
